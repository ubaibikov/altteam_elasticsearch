<?php

namespace Tygh\Addons\AltteamElasticSearch;

use Elasticsearch\ClientBuilder;

class ElasticSearch
{
    private $elasticSearchClient;
    private $index;
  
    public function __construct(string $index)
    {
        $this->index = $index;        
    }
    public function connect()
    {
        $client = ClientBuilder::create()
            ->setHosts($this->getElasticSearchHostSettings())
            ->build();
        $this->elasticSearchClient = $client;
    }

    protected function getElasticSearchHostSettings(): array
    {
        return fn_get_altteam_elasticsearch_settings();
    }

    public function creteElasticSearchIndex()
    {
        $this->elasticSearchClient->indicies()->create(['index' => $this->index]);
    }

    public function getSettingsElasticSearch()
    {
        return $this->elasticSearchClient->indicies()->getSettings($this->params);
    }

    public function createIndexingElasticSearch()
    {
        $params = [
            'index' => 'products',
            'id' => 1,
            'body' => [
                'text' => 'blob livak facked'
            ],
        ];

        $this->elasticSearchClient->index($params);
    }

    public function isExistsElasticSearchIndex()
    {
        return $this->elasticSearchClient->exists($this->index) ?? false;
    }
    
    public function getElasticSearchProductIndex(string $productId)
    {
        $getParams = [
            'index' => 'products',
            'id' => $productId
        ];

        return $this->elasticSearchClient->get($getParams);
    }

    public function getIndexProductData(string $productId, array $productData): array
    {
        return [
            'index' => $this->index,
            'id' => $productId,
            'body' => self::buildIndexBodyProductData($productData),
        ];
    }

    public static function buildIndexBodyProductData(array $prodcutData): array
    {
        $buildedData = [];
        foreach ($prodcutData as $productDataKey => $data) {
            $buildedData[$productDataKey] = $data;
        }
        return $buildedData;
    }

    public static function trimElasticSearchSettings(string $value): string
    {
        return trim($value);
    }
}
