<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/
use Tygh\Registry;
use Tygh\Addons\AltteamElasticSearch\ElasticSearch;

if (!defined('BOOTSTRAP')) { die('Access denied');}

function fn_get_altteam_elasticsearch_settings()
{
    $settings = Registry::get('addons.altteam_elasticsearch');
    $elastic_search = ElasticSearch::class;
    
    return [
        $elastic_search::trimElasticSearchSettings($settings['elasticsearchIpwithPort']),
        $elastic_search::trimElasticSearchSettings($settings['storeIp']),
        $elastic_search::trimElasticSearchSettings($settings['elastsearchDomainWithPort']),
        $elastic_search::trimElasticSearchSettings($settings['storeDomain']),
        $elastic_search::trimElasticSearchSettings($settings['SSL']),
        $elastic_search::trimElasticSearchSettings($settings['elasticSearchDomainWithPort']),
    ];
}

function fn_altteam_elasticsearch_get_search_data(array $product_data):array
{
    return [
        'product' => $product_data['product'],
        'meta_keywords' => $product_data['meta_keywords'],
        'product_code' => $product_data['product_code'],
        'short_description' => htmlentities($product_data['short_description'],ENT_QUOTES, 'UTF-8'),
        'full_description' => htmlentities($product_data['full_description'],ENT_QUOTES, 'UTF-8')
    ];
}


function fn_altteam_elasticsearch_update_product_pre($product_data, $product_id, $lang_code, $can_update)
{

    $elastic_search = new ElasticSearch('products');
    $elastic_search->connect();

    if($elastic_search->isExistsElasticSearchIndex()){
        $elastic_search->creteElasticSearchIndex(); 
    }
}
