<?php

/*****************************************************************************
 * This is a commercial software, only users who have purchased a  valid
 * license and accepts the terms of the License Agreement can install and use  
 * this program.
 *----------------------------------------------------------------------------
 * @copyright  LCC Alt-team: http://www.alt-team.com
 * @license    http://www.alt-team.com/addons-license-agreement.html
****************************************************************************/


if (!defined('BOOTSTRAP')) { die('Access denied'); }

require_once __DIR__ . '/lib/vendor/autoload.php';

fn_register_hooks(
    "update_product_pre"
);
